
import React,{Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Form,Button}from 'react-bootstrap';

let index=1;
class Calculate extends Component{
    constructor(props)
    {
        super(props)
            this.state={
                num1:'',
                num2:'',
                cal:'+'
            }
    }

    handleChange =(e)=>
    {
        this.setState(
            {
                [e.target.name]:e.target.value
            }
        )
    }
    onCalculate=(e)=>
    {
        let result=0;
        if(this.state.num1==="")
        {
            alert("Please input num1:");
            return;
        }
        else if(this.state.num2==="")
        {
            alert("Please input num2:");
            return;
        }

        e.preventDefault();
        const form=
        {
            num1:this.state.num1,
            num2:this.state.num2,
            cal:this.state.cal
        }
        var number1=parseInt(form.num1);
        var number2=parseInt(form.num2);
        
        if(form.cal==="+")
        {
            result=number1+number2;
        }
        else if(form.cal==="-")
        {
            result=number1-number2;
        }
        else if(form.cal==="*")
        {
            result=number1*number2;
        }
        else if(form.cal==="/")
        {
            result= number1/number2;
        }
        else if(form.cal==="%")
        {
            result=number1 % number2;
        }
        var myObj=
        {
            id:index++,
            myresult:result
        }
        this.props.dataFromParent(myObj);
    }

        render(){
            return(

                <div>
                    <Form>
                    <Form.Group>
                        <Form.Control type="text" name="num1" value={this.state.num1} onChange={e=>this.handleChange(e)} placeholder="Enter number1" style={{width: "500px"}}></Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Control type="text" name="num2" value={this.state.num2} onChange={e=>this.handleChange(e)} placeholder="Enter number2" style={{width: "500px"}}></Form.Control>
                    </Form.Group>
                    <Form.Group> 
                        <Form.Control as="select" name="cal" onChange={e=>this.handleChange(e)} value={this.state.cal} className="control" style={{width: "500px"}}>
                            <option value="+">+ Plus</option>
                            <option value="-">- Sub</option>
                            <option value="*">* Mul</option>
                            <option value="/">/ Divide</option>
                            <option vlaue="%">% Modulus</option>
                        </Form.Control>

                    </Form.Group>
                        <Button type="button"  onClick={
                            (e)=>this.onCalculate(e)
                        }>Calculate</Button>
                    </Form>
                </div>

            )
        }
    
    }


export default Calculate;














