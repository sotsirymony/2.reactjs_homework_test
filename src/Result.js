import React from 'react'
import ReactDOM from 'react-dom';  

function Result(props)
{
    let ResultNumbers=( props.result);
    let listItems =ResultNumbers.map((number)=>
    <li key={number.id}>{number.myresult}</li>
);
return (
    <div>
        <h2>The Result History</h2>
        <ul>
            {listItems}
        </ul>
    </div>
)
}
export default Result;