import React, { Component,renders } from 'react';
import Calculate from'./Calculate';
import Result from './Result';
import 'bootstrap/dist/css/bootstrap.min.css';

import './App.css';


class App extends Component
{
  constructor(props)
  {
    super(props)
      this.state={
        resultVal:[]
      }
    
  }

handleResult(val)
{
  console.log(val)
    this.setState({
      resultVal:[...this.state.resultVal,val]
  })
  
}

render(){
  return (
    <div>
      <div className="my-container">
        <div className="container">
            <div className="row">
                <div className="col-md-12">
                      
                </div>
            </div>
            <div className="row">
                <div className="col-md-6" id="calculate">
                <img src="/images/icon.png"/>
                  <Calculate dataFromParent={
                    this.handleResult.bind(this)
                  }/>
                </div>
                <div className="col-md-6" id="result">
                    <Result result={this.state.resultVal}/>
                </div>
            </div>
        </div>
      </div>
    
    </div>
  
  )
                
}
}




export default App;

